const strings = {
  en: {
    about: 'About René Magritte',
    painting: 'The Painting',
    artist: 'The Artist',
    notAPipe: 'This is not a pipe',
    bio: `René François Ghislain Magritte (21 November 1898 – 15 August 1967) was a Belgian Surrealist artist. He became well known for creating a number of witty and thought-provoking images. Often depicting ordinary objects in an unusual context, his work is known for challenging observers' preconditioned perceptions of reality. His imagery has influenced pop art, minimalist art and conceptual art.`,
    readMore: 'Read more on Wikipedia...',
    title:"Personalize your Twitch or Youtube design",
    intro:"And when we say “personalize” we say that we would like to focus on your wishes and moods regarding visuals, colors, animations… in order to create a full original and unique design.",
    users:[
      {
        id: 1,
        title: 'Pack Simple 50 €',
        description:"test",
      },
      { id: 2, title: 'Pack Extra 250 €', description:"test"  },
    ]
  },
  fr: {
    about: 'Sur René Magritte',
    painting: 'La peinture',
    artist: "L'Artiste",
    notAPipe: "Ceci n'est pas une pipe",
    bio: `René Magritte est un peintre surréaliste belge, né le 21 novembre 1898 à Lessines1 dans le Hainaut (Belgique) et mort à Schaerbeek le 15 août 1967.`,
    readMore: 'Lire plus sur Wikipedia...',
    title:"Personalize your Twitch or Youtube design",
    intro:"And when we say “personalize” we say that we would like to focus on your wishes and moods regarding visuals, colors, animations… in order to create a full original and unique design.",
    users:[
      { id: 1, title: 'Pack Simple 50 €', description:"testtest" },
      { id: 2, title: 'Pack Extra 250 €', description:"testtest"  },
    ]
  },
  pl: {
    about: 'O René Magritte',
    painting: 'Obraz',
    artist: 'Artysta',
    notAPipe: 'To nie jest fajka',
    bio: `René François Ghislain Magritte (ur. 21 listopada 1898 w Lessines, Hainaut, zm. 15 sierpnia 1967 w Brukseli) – belgijski malarz, surrealista. Nazywany "autorem słów i rzeczy", lubił zmieniać nazwy przedmiotów, czego przykładem mógłby być obraz Zdradliwość obrazów. Obrazy jego szokowały i wzbudzały kontrowersje, wyróżnia je poetycka nastrojowość oraz precyzyjny rysunek. Artysta, który przedstawiał świat zuniformizowany, odbity w kolejnych powieleniach, wpłynął mimowolnie na sztukę drugiej połowy XX w., przede wszystkim na pop-art.`,
    readMore: 'Dowiedz się więcej z Wikipedii...',
    title:"Personalize your Twitch or Youtube design",
    intro:"And when we say “personalize” we say that we would like to focus on your wishes and moods regarding visuals, colors, animations… in order to create a full original and unique design.",
    users:[
      { id: 1, title: 'Pack Simple 50 €', description:"testtesttest" },
      { id: 2, title: 'Pack Extra 250 €', description:"testtesttest"  },
    ]
  }
}

export default strings
