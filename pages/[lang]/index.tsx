import React from 'react'
import Layout from '../../components/Layout'
import Painting from '../../components/Painting'
import withLocale from '../../hocs/withLocale'
import Offers from '../../components/Offers'
import Intro from '../../components/Intro'

const IndexPage: React.FC = () => {
  return (
    <Layout titleKey="notAPipe">
      <Intro />
      <Offers />
    </Layout>
  )
}

export default withLocale(IndexPage)
