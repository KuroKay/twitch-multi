import React from 'react'
import useTranslation from '../hooks/useTranslation'

const Intro: React.FC = () => {
  const { locale, t } = useTranslation()
  return (
    <section className="intro">
        <div className="intro-text">
            <h1>{t('title')}</h1>
            <p>{t('intro')}</p>
        </div>
        <img className="image-gaming" src="/img/gaming.jpg" alt="gaming" />
        <style jsx>{`
            .intro{
                position:relative;
            }
            .image-gaming{
                width:100%;
            }
            .intro-text{
                position:absolute;
                color:#fff;
                box-sizing: border-box;
                padding: 10% 10% 5% 5%;
                background: linear-gradient(90deg, rgba(0,0,0,1) 0%, rgba(0,0,0,1) 78%, rgba(0,0,0,0) 100%);
                height:100%;
                width:50%;
            }
        `}</style>
    </section>
  )
}

export default Intro
