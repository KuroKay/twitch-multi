import React from 'react'
import useTranslation from '../hooks/useTranslation'
import OffersList from './OffersList'

const Offers: React.FC = () => {
  const { locale, t } = useTranslation()
  return (
    <section className="offers">
        <div className="userList">
            {t('users').map(({ id, title, description }) => (
                <>
                    <h3 key={id}>{title}</h3>
                    <p>{description}</p>
                </>
            ))};
        </div>
    </section>
  )
}

export default Offers
